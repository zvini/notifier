#!/bin/bash
cd `dirname $BASH_SOURCE`
if [ -z $DISPLAY ]
then
    export DISPLAY=:0
fi
./stop.sh
./program.js > program.out 2> program.err &
echo $! > program.pid
sleep 0.2
cat program.out program.err
