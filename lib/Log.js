module.exports = function (text) {
    var date = new Date,
        year = date.getFullYear(),
        month = TwoDigitPad(date.getMonth() + 1),
        day = TwoDigitPad(date.getDate()),
        hour = TwoDigitPad(date.getHours()),
        minute = TwoDigitPad(date.getMinutes()),
        second = TwoDigitPad(date.getSeconds())
    console.log(year + '-' + month + '-' + day + ' ' +
        hour + ':' + minute + ':' + second + ' ' + text)
}

var TwoDigitPad = require('./TwoDigitPad.js')
