#!/usr/bin/env node

process.chdir(__dirname)

var child_process = require('child_process'),
    Log = require('./lib/Log.js')

var notifySend = child_process.spawn('notify-send')
notifySend.on('error', function (err) {
    if (err.code == 'ENOENT') {
        Log('ERROR: "notify-send" command not found.')
    } else {
        throw err
    }
})
notifySend.on('exit', function () {

    function check () {

        var req = createRequest(requestOptions, function (res) {
            var response
            var responseText = ''
            res.setEncoding('utf8')
            res.on('error', function (err) {
                Log('ERROR: Failed to read response. Error code: ' + err.code)
                schedule()
            })
            res.on('data', function (data) {
                responseText += data
            })
            res.on('end', function () {

                schedule()

                try {
                    response = JSON.parse(responseText)
                } catch (e) {
                    Log('ERROR: Expected JSON document but received something else: ' + responseText)
                    return
                }

                if (res.statusCode == 200) {
                    if (response instanceof Array) {
                        response.reverse().forEach(function (notification) {
                            var id = notification.id
                            if (id > lastNotificationId) {
                                lastNotificationId = id
                                var notifySend = child_process.spawn('notify-send', [
                                    '-t', '0',
                                    notification.channel_name,
                                    notification.text,
                                ])
                                notifySend.on('error', function (err) {
                                    if (err.code == 'ENOENT') {
                                        Log('ERROR: "notify-send" command not found.')
                                    } else {
                                        throw err
                                    }
                                })
                            }
                        })
                    } else {
                        Log('ERROR: Invalid response received: ' + response)
                    }
                } else {
                    if (response == 'INVALID_API_KEY') {
                        Log('ERROR: The API key is invalid.')
                        process.exit(1)
                    } else {
                        Log('ERROR: Unknown error received: ' + response)
                    }
                }

            })
        })
        req.end()

        req.on('error', function (err) {
            Log('ERROR: Request failed. Error code: ' + err.code)
            schedule()
        })

    }

    function schedule () {
        setTimeout(check, 60000)
    }

    var lastNotificationId = 0

    var config = require('./config.js')

    var base = config.base || '/',
        host = config.host || 'localhost',
        protocol = config.protocol || 'http',
        port = config.port || (protocol == 'http' ? 80 : 443)

    var createRequest
    if (protocol == 'http') createRequest = require('http').request
    else createRequest = require('https').request

    var requestOptions = {
        host: host,
        port: port,
        path: base + 'api-call/notification/list?api_key=' + encodeURIComponent(config.api_key),
        headers: { host: host },
    }

    check()

})
