Zvini Notifier
==============

A program that runs in background and checks for new
notifications on a [Zvini](https://github.com/zvini/website) instance.

Scripts
-------

* `./restart.sh` - start/restart the program.
* `./stop.sh` - stop the program.

Configuration
-------------

`config.js` contains the configuration. See `example-config.js` for details.

See Also
--------

* [Zvini Website](https://github.com/zvini/website)
